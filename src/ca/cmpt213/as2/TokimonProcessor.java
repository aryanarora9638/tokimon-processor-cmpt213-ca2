package ca.cmpt213.as2;

import com.google.gson.Gson;

import java.io.*;
import java.net.URI;
import java.nio.file.*;

public class TokimonProcessor {
    public static void main(String[] args) throws IOException {
        ReadJsonFiles readJsonFiles = new ReadJsonFiles();
        readJsonFiles.readFileNamesIntoList();
        System.out.println("Total files to be read - " + ReadJsonFiles.toReadFile.size());
        System.out.println("All data has been read - " + ReadJsonFiles.jsonFilesData.size());

        GroupingData groupingData = new GroupingData();
        groupingData.groupData();

        WriteCsvFiles.generateCsvFilesData();
    }
}