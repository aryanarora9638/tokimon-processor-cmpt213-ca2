package ca.cmpt213.as2;

import java.io.FileWriter;
import java.io.IOException;

public class WriteCsvFiles {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private static final String FILE_HEADER = "Team#, From Toki, To Toki, , Score, Comment, , Extra";

    public static String fileName = "team_info_Sample.csv";

    public static void generateCsvFilesData() throws IOException {
        FileWriter fileWriter = null;

        //CSV file formatting tools
        String NEW_LINE_SEPARATOR = "\n";
        String FILE_HEADER = "Team#, From Toki, To Toki, , Score, Comment, , Extra";
        String fileName = "team_info_Sample.csv";
        char teamNumber = 'x';


        try{
            //file headers
            fileWriter = new FileWriter(fileName);
            fileWriter.append(FILE_HEADER);
            fileWriter.append(NEW_LINE_SEPARATOR);

            System.out.println("Writing files....");
            for (int teamItem = 0 ; teamItem < GroupingData.ArrangedData.size(); teamItem++) {
                int firstItemOfTheTeam = 0;
                //check team numbers
                char newTeamNumber = GroupingData.getTeamNumber(teamItem, GroupingData.ArrangedData).charAt(1);
                if(newTeamNumber != teamNumber){
                    fileWriter.append("Team" + newTeamNumber);
                    fileWriter.append(NEW_LINE_SEPARATOR);
                    teamNumber = newTeamNumber;
                }
                else {
                    fileWriter.append(NEW_LINE_SEPARATOR);
                }
                System.out.println("Team number is - " + teamNumber);

                //write team-member information
                for (int memberInfo = 0; memberInfo < GroupingData.ArrangedData.get(teamItem).team.size(); memberInfo++) {
                    if (memberInfo >= 0) {
                        //not-captain
                        fileWriter.append("");
                        fileWriter.append(COMMA_DELIMITER);
                        //From Toki
                        System.out.println("Captain is - " + GroupingData.ArrangedData.get(teamItem).team.get(firstItemOfTheTeam).id.toString());
                        fileWriter.append(GroupingData.ArrangedData.get(teamItem).team.get(firstItemOfTheTeam).id.toString());
                        fileWriter.append(COMMA_DELIMITER);
                        //To Toki
                        fileWriter.append(GroupingData.ArrangedData.get(teamItem).team.get(memberInfo).id.toString());
                        fileWriter.append(COMMA_DELIMITER);
                        //empty space
                        fileWriter.append("");
                        fileWriter.append(COMMA_DELIMITER);
                        //Score
                        fileWriter.append(String.valueOf(GroupingData.ArrangedData.get(teamItem).team.get(memberInfo).compatibility.score));
                        fileWriter.append(COMMA_DELIMITER);
                        //Comment
                        fileWriter.append(GroupingData.ArrangedData.get(teamItem).team.get(memberInfo).compatibility.comment);
                        fileWriter.append(COMMA_DELIMITER);
                        //empty space
                        fileWriter.append("");
                        fileWriter.append(COMMA_DELIMITER);
                        fileWriter.append(NEW_LINE_SEPARATOR);
                    }
                }
                //Captain sign off, write captain/owner information
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(GroupingData.ArrangedData.get(teamItem).team.get(firstItemOfTheTeam).id.toString());
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append("-");
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(String.valueOf(GroupingData.ArrangedData.get(teamItem).team.get(firstItemOfTheTeam).compatibility.score));
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(GroupingData.ArrangedData.get(teamItem).team.get(firstItemOfTheTeam).compatibility.comment);
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(GroupingData.ArrangedData.get(teamItem).extraComments);
                fileWriter.append(NEW_LINE_SEPARATOR);
            }
            System.out.println("Writing files finnished...");
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }
    }
}
