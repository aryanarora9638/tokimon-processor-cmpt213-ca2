package ca.cmpt213.as2;

import java.util.ArrayList;

public class GroupingData {

    public static ArrayList<TokimonTeam> ArrangedData = new ArrayList<>();

    public void groupData(){
        while(ReadJsonFiles.jsonFilesData.size() != 0){
            int teamItem = 0;
            String teamNumber = getTeamNumber(teamItem, ReadJsonFiles.jsonFilesData);
            System.out.println("Found team #" + teamNumber);
            for(int sameTeamItem = 0; sameTeamItem < ReadJsonFiles.jsonFilesData.size(); sameTeamItem++){
                if(teamNumber.equalsIgnoreCase(getTeamNumber(sameTeamItem, ReadJsonFiles.jsonFilesData))){
                    ArrangedData.add(ReadJsonFiles.jsonFilesData.get(sameTeamItem));
                    ReadJsonFiles.jsonFilesData.remove(sameTeamItem);
                }
            }
        }
        System.out.println("All data grouped along with similar teams");
        System.out.println(ArrangedData.size());
    }

    public static String getTeamNumber(int teamItem, ArrayList<TokimonTeam> jsonFilesData){
        String teamNumber = "";
        int firstItemOfTheTeam = 0;
        int startIndex = jsonFilesData.get(teamItem).team.get(firstItemOfTheTeam).id.indexOf("-");
        teamNumber = jsonFilesData.get(teamItem).team.get(firstItemOfTheTeam).id.substring(startIndex+1, startIndex+3).trim();
        return teamNumber;
    }

}
