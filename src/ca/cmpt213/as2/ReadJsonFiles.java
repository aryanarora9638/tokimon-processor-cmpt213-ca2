package ca.cmpt213.as2;

import com.google.gson.*;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class ReadJsonFiles {

    public static ArrayList<File> toReadFile = new ArrayList<>();
    public static ArrayList<TokimonTeam> jsonFilesData = new ArrayList<>();

    private static void dumpFiles() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonParser jsonParser = new JsonParser();
        String json = "";
        JsonElement jsonElement;


        for(File subFile : toReadFile){
            System.out.println("Found tokimon team - reading data....");
            System.out.println(" file: " + subFile.getAbsoluteFile());
            TokimonTeam tokimonTeam = new TokimonTeam();
//            json = gson.toJson(subFile.toString());
            JsonObject jsonObject = (JsonObject) jsonParser.parse(new FileReader(subFile));
            JsonArray jsonArray = (JsonArray) jsonObject.get("team");
            for(JsonElement x : jsonArray){
                System.out.println("New tokimon added to team");

                JsonElement name =  x.getAsJsonObject().get("name");
                JsonElement id = x.getAsJsonObject().get("id");
                JsonElement score = x.getAsJsonObject().get("compatibility").getAsJsonObject().get("score");
                JsonElement comment = x.getAsJsonObject().get("compatibility").getAsJsonObject().get("comment");

                String jsonName = name.toString();
                String jsonId = id.toString().trim();
                double jsonScore = score.getAsDouble();
                String jsonComment = comment.toString();

                tokimonTeam.addTeamMember(jsonName,jsonId,jsonScore,jsonComment);
            }

            JsonElement extraComment = jsonObject.get("extra_comments");
            String jsonExtraComment = extraComment.toString();

            tokimonTeam.extraComments = jsonExtraComment;
            System.out.println("All Tokimons added to team");
            jsonFilesData.add(tokimonTeam);
        }

    }

    public  void readFileNamesIntoList() throws FileNotFoundException {//handle exception and errors
        File testFile = new File("testdata/InputTestDataSets");
        test(testFile,"json", toReadFile);
        dumpFiles();
    }

    public File test(File file, String search, ArrayList<File> toReadFile){
        if(file.isDirectory()) {
            File[] newFiles = file.listFiles();
            for (File test : newFiles) {
                File found = test(test, search, toReadFile);
                if (test.getAbsolutePath().contains("json")) {
                    toReadFile.add(found);
                    return found;
                }
            }
        }
        else {
            if(file.getAbsolutePath().contains("json")){
                return file;
            }
        }
        return null;
    }
}
