package ca.cmpt213.as2;

import java.util.ArrayList;
import java.util.List;

public class TokimonTeam {
    public ArrayList<MemberInfo> team;
    public String extraComments;

    public TokimonTeam() {
        this.extraComments = "extra comments";
        this.team = new ArrayList<MemberInfo>();
    }

    public void addTeamMember(String name, String id, double score, String comment){
        MemberInfo memberInfo = new MemberInfo(name, id,score, comment);
        this.team.add(memberInfo);

    }
}

 class MemberInfo {
    public String name;
    public String id;
    public Compatibility compatibility;

    public MemberInfo(String name, String id, double score, String comment){
        this.name = name;
        this.id = id;
        this.compatibility = new Compatibility(score,comment);
    }
 }


class Compatibility {
    public double score;
    public String comment;

    public Compatibility(double score, String comment){
        this.score = score;
        this.comment = comment;
    }
}


